// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTanks.h"
#include "TankAIController.h"


ATank* ATankAIController::GetControlledTank() const
{
	ATank* t = Cast<ATank>(GetPawn());
	return t;
}

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();

	auto controlledTank = GetControlledTank();

	if (controlledTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("AITank => %s "), *controlledTank->GetName());
	}
}

