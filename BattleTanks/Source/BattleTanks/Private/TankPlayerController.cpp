// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTanks.h"
#include "TankPlayerController.h"


ATank* ATankPlayerController::GetControlledTank() const
{
	ATank* t = Cast<ATank>(GetPawn());
	return t;
}

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();

	auto controlledTank = GetControlledTank();

	if (controlledTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("ATankPlayerController => %s"), *controlledTank->GetName());
	}
	else 
	{
		UE_LOG(LogTemp, Warning, TEXT("No ATankPlayerController was found!! make sure you are using the 'Tank' class"));
	}


	
}